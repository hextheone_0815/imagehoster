package com.ibucket.managedbeans;

import com.ibucket.businesslayer.AnonUser;
import com.ibucket.businesslayer.AuthUser;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 * 
 * Bean für Loginform, beinhaltet Sessiondaten
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {
    
    // Anonyme Userfunktionen
    AnonUser anon;
    String logon;
    String password;
    
    /**
     * Angemeldeter Nutzer samt Funktionen
     **/
    AuthUser usr;

    /**
     * Initieren einer neuen Instanz
     */
    public LoginBean()
    {
        anon = new AnonUser();
        usr = null;
    }
    
    /**
     * Check ob Sessionuser angemldet ist
     * @return true wenn nicht Angemeldet
     */
    public boolean isNotLoggedIn()
    {
        boolean var = usr == null;
        return var;
    }
    
    /**
     * Anmelden mit gegeben Daten vom Webinterface
     * @return 
     */
    public String loginAction()
    {
        usr = anon.login(logon, password);
        if (usr == null)
        {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Login fehlgeschlagen!","Nutzername oder Passwort falsch."));
        } else {
            logon = usr.getLogon();
        }
        return "index";
    }
    
    /**
     * Abmelden des Benutzers
     * @return Startseite
     */
    public String logoutAction()
    {
        this.usr.logout();
        this.usr =null;
        this.logon = "";
        this.password ="";
        return "index";

    }
    
    
    public void setLogon(String logon) {
        this.logon = logon;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogon() {
        return logon;
    }

    public String getPassword() {
        return null;
    }
    /**
     * Prüft ob das Bild dem ANgemldeten Nutzer gehört
     * @param imgId
     * @return true wenn Angemldeter Benutzer der Besitzer ist
     */
    public boolean ownsPicture(String imgId) {
        return usr.ownsPicture(imgId) != null;
    }
    
    /**
     * Prüft ob Galerie dem Nutzer gehört
     * @param galleryId
     * @return true wenn Angemldeter Benutzer der Besitzer der Galerie ist
     */
    public boolean ownsGallery(String galleryId) {
        return usr.ownsGallery(Integer.parseInt(galleryId)) != null;
    }
    
    /**
     * Auffrischen der Benutzerdaten der Session
     */
    public void refresh() {
        usr = anon.login(logon, password);
    }
}
