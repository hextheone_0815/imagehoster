package com.ibucket.managedbeans;

import com.ibucket.persistence.Gallery;
import com.ibucket.persistence.Picture;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * 
 * Bean für Gallery
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@ManagedBean(name = "galleryBean")
@RequestScoped
public class GalleryBean {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    private String galleryName;
    private List<Gallery> galleries;
    private Map<Integer, List<String>> galleryNumbers = new HashMap<>();
    private Map<Integer, List<String>> galleryPictureDescriptions = new HashMap<>();

    /**
     * Creates a new instance of GalleryBean
     */
    public GalleryBean() {
    }
    
    /**
     * Initialisiert Klassenvariablen
     */
    @PostConstruct
    public void init() {
        galleryName = "";
        galleries = null;
        galleryNumbers = new HashMap<>();
        galleryPictureDescriptions = new HashMap<>();
        if (!loginBean.isNotLoggedIn()) {
            galleries = loginBean.usr.getAllGalleries();
            for (Gallery g : galleries) {
                List<Picture> pictures = new ArrayList<>(g.getPictureCollection());
                Collections.sort(pictures, new Comparator<Picture>() {
                    @Override
                    public int compare(Picture p1, Picture p2) {
                        return p1.getNr()-p2.getNr();
                    }
                });
                List<String> pictureDescriptions = new ArrayList<>();
                List<String> numbers = new ArrayList<>();                           
                for (Picture p : pictures) {
                    numbers.add(Integer.toString(p.getPictureId()));
                    pictureDescriptions.add(p.getDescription());
                }
                galleryPictureDescriptions.put(g.getGalleryId(), pictureDescriptions);
                galleryNumbers.put(g.getGalleryId(), numbers);
            }
        }
    }
    
    public String getLink(String id) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/javax.faces.resource/dynamiccontent.properties.xhtml?ln=primefaces&pfdrid=VyNz3i0uOYnQ9Uc0DFN1sg2e%2B2MD5MrJ&imageID=" + id + "&pfdrid_c=true";
    }
    
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public String getGalleryName() {
        return galleryName;
    }

    public void setGalleryName(String galleryName) {
        this.galleryName = galleryName;
    }

    /**
     * Erstellt eine neue Galerie.
     * @return Navigation-String "gallery" um auf die gallery.xhtml weiterzuleiten
     */
    public String createGallery() {
        loginBean.usr.createGallery(galleryName);
        loginBean.refresh();
        init();
        return "gallery";
    }
    
    public List<Gallery> getGalleries() {
        return galleries;
    }

    public Map<Integer, List<String>> getGalleryNumbers() {
        return galleryNumbers;
    }   

    public Map<Integer, List<String>> getGalleryPictureDescriptions() {
        return galleryPictureDescriptions;
    }
    
    /**
     * Löscht eine Gallerie
     * @param galleryId Id der zu löschenden Galerie
     * @return Navigation-String "gallery" für die Weiterleitung zur gallery.xhtml
     */
    public String deleteGallery(int galleryId) {
        loginBean.usr.deleteGallery(galleryId);
        loginBean.refresh();
        init();
        return "gallery";
    }

    /**
     * Erstellt eine Gallerie
     * @param galleries 
     */
    public void setGalleries(List<Gallery> galleries) {
        this.galleries = galleries;
    }
    
    public String getDescription(String i) {        
        Picture picture = loginBean.usr.ownsPicture(i);
        
        return picture.getDescription();
    }    
    
    /**
     * Holt die Bilder einer Gallerie
     * @return StreamedContent-Objekt des Bildes, wenn das Bild öffentlich ist oder 
     * dem Nutzer gehört
     * @throws FileNotFoundException
     * @throws URISyntaxException 
     */
    public StreamedContent getImage() throws FileNotFoundException, URISyntaxException {
        if (FacesContext.getCurrentInstance().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        }
        else {
            String i = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("imageID");
            StreamedContent stream = null;
            Picture picture = loginBean.anon.isPicturePublic(Integer.parseInt(i));
            if (picture != null) {               
                stream = readFile(picture);
            }
            else {
                picture = loginBean.usr.ownsPicture(i);
                if (picture != null) {
                    stream = readFile(picture);
                }
                else {
                    stream = new DefaultStreamedContent();
                }
            }     
            return stream;
        }
    }
    
    /**
     * Streamt ein Bild
     * @param picture Datenbankobjekt des Bildes
     * @return Bild als StreamedContent-Objekt oder leeres StreamedContent-Objekt, wenn das Bild nicht gefunden wurde.
     */
    private StreamedContent readFile(Picture picture) {
        try {
            String filename = Integer.toString(picture.getFilename()) + "." + picture.getFiletype();
            File pictureFile = new File("../pictures/" + filename); 
            return new DefaultStreamedContent(new FileInputStream(pictureFile), "image/" + picture.getFiletype().toLowerCase(), filename);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new DefaultStreamedContent();
    }
    
    /**
     * Listener, der auf die gallery.xhtml weiterleitet, wenn der Optionsdialog geschlossen wird.
     * @throws IOException 
     */
    public void dialogReturnListener() throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect("gallery.xhtml");
    }
}
