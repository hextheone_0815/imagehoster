package com.ibucket.managedbeans;

import com.ibucket.persistence.Gallery;
import com.ibucket.persistence.databaseutil.DatabaseUtilRemote;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 * 
 * Bean für die Gallerieeinstellungen
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@ManagedBean( name = "galleryConfigBean")
@RequestScoped
public class GalleryConfigBean {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    private String description;
    private String galleryName;
    private Character status;
    private Character statusPublic;
    private Character statusPrivate;
    private Gallery gallery;
    private String galleryId;    
    
    public GalleryConfigBean() {
        
    }
    
    /**
     * Holt die Einstellungen fur die Gallerie
     * @param id 
     */
    public void viewConfig(String id) {
        Map<String,Object> options = new HashMap<>();
        options.put("modal", true);
        options.put("draggable", true);
        options.put("resizable", false);
        options.put("contentHeight", 400);
        options.put("contentWidth", 400);
        Map<String, List<String>> params = new HashMap<>();
        List<String> paramList = new ArrayList<>();
        paramList.add(id);
        params.put("galleryID", paramList);
        RequestContext.getCurrentInstance().openDialog("galleryconfig", options, params);
    }
    
    private void closeConfig() {
        RequestContext.getCurrentInstance().closeDialog(null);
    }

    @PostConstruct
    public void init() {
        statusPublic = DatabaseUtilRemote.PUBLIC;
        statusPrivate = DatabaseUtilRemote.PRIVATE;
        galleryId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("galleryID");
        gallery = loginBean.usr.ownsGallery(Integer.parseInt(galleryId));
        if (gallery == null) {
            throw new IllegalArgumentException();
        }
        description = gallery.getDescription();
        status = gallery.getVisibilityStatus();
        galleryName = gallery.getName();
    }

    /**
     * Speichert Gallerieeinstellungen
     */
    public void save() {
        loginBean.usr.setGalleryDescription(gallery.getGalleryId(), description);
        if (Objects.equals(status, statusPrivate)) {
            loginBean.usr.unpublishGallery(gallery.getGalleryId());           
        }
        else if (Objects.equals(status, statusPublic)) {
            loginBean.usr.publishGallery(gallery.getGalleryId());
        }
        loginBean.usr.setGalleryName(gallery.getGalleryId(), galleryName);
        
        loginBean.refresh();
        closeConfig();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public Character getStatusPublic() {
        return statusPublic;
    }

    public Character getStatusPrivate() {
        return statusPrivate;
    }

    public String getGalleryName() {
        return galleryName;
    }

    public void setGalleryName(String galleryName) {
        this.galleryName = galleryName;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getGalleryId() {
        return galleryId;
    }

    public void setGalleryId(String galleryId) {
        this.galleryId = galleryId;
    }        
}
