package com.ibucket.managedbeans;

import com.ibucket.persistence.Gallery;
import com.ibucket.persistence.Picture;
import com.ibucket.persistence.databaseutil.DatabaseUtilRemote;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 * 
 * Bean für Bildeinstellungen
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@ManagedBean( name = "imageConfigBean")
@RequestScoped
public class ImageConfigBean {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    private String description;
    private Character status;
    private Integer gallery;
    private Character statusPublic;
    private Character statusPrivate;
    private String imageId;    
    private int nr;
    
    private Picture picture;
    
    public ImageConfigBean() {
    }
    
    /**
     * Öffnet das Einstellungsmenu
     * @param id 
     */
    public void viewConfig(String id) {
        Map<String,Object> options = new HashMap<>();
        options.put("modal", true);
        options.put("draggable", true);
        options.put("resizable", false);
        options.put("contentHeight", 440);
        options.put("contentWidth", 400);
        Map<String, List<String>> params = new HashMap<>();
        List<String> paramList = new ArrayList<>();
        paramList.add(id);
        params.put("imageID", paramList);
        RequestContext.getCurrentInstance().openDialog("pictureconfig", options, params);
    }
    
    private void closeConfig() {
        RequestContext.getCurrentInstance().closeDialog(null);
    }

    @PostConstruct
    public void init() {
        statusPublic = DatabaseUtilRemote.PUBLIC;
        statusPrivate = DatabaseUtilRemote.PRIVATE;
        imageId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("imageID");
        picture = loginBean.usr.ownsPicture(imageId);
        if (picture == null) {
            throw new IllegalArgumentException();
        }
        nr = picture.getNr();
        gallery = picture.getGallery()==null?0:picture.getGallery().getGalleryId();
        description = picture.getDescription();
        status = picture.getVisibilityStatus();
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Integer getGallery() {
        return gallery;
    }

    public void setGallery(Integer gallery) {
        this.gallery = gallery;
    }

    public Character getStatusPublic() {
        return statusPublic;
    }

    public Character getStatusPrivate() {
        return statusPrivate;
    }

    public Picture getPicture() {
        return picture;
    }        
    
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }        
    
    public List<Gallery> getGalleries() {
        return loginBean.usr.getAllGalleries();
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }    

    /**
     * Speichert Daten in der Datenbank
     */
    public void save() {
        loginBean.usr.setPictureDescription(picture.getPictureId(), description);
        if (Objects.equals(status, statusPublic)) {
            loginBean.usr.publishPicture(picture.getPictureId());
        }
        else {
            loginBean.usr.unpublishPicture(picture.getPictureId());
        }
        loginBean.usr.changePictureNumber(picture.getPictureId(), nr);
        loginBean.usr.changePictureGallery(gallery, picture.getPictureId());
        loginBean.refresh();
        closeConfig();
    }
}
