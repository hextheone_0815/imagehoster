package com.ibucket.managedbeans;

import com.ibucket.persistence.Picture;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * 
 * Bean für Bilder
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */

@ManagedBean(name="imageBean")
@RequestScoped
public class ImageBean implements Serializable {

    @ManagedProperty( value = "#{loginBean}" )
    private LoginBean loginBean;
    private List<Picture> images;
    private List<Integer> imageNumbers;

    public ImageBean() {
        
    }
    
    @PostConstruct
    public void init() {
        if (loginBean.usr != null) {
            images = loginBean.usr.getAllPictures();
            imageNumbers = new ArrayList<>();
            for (Picture p : images) {
                imageNumbers.add(Integer.valueOf(p.getPictureId()));
            }
        }
    }

    public String getLink(String id) {        
        return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/javax.faces.resource/dynamiccontent.properties.xhtml?ln=primefaces&pfdrid=wSMrNu61xjs6vWbBkJoYALKZGsyJ2d7d&imageID=" + id + "&pfdrid_c=true";
    }
    
    public List<Picture> getImages() {
        return images;
    }

    public List<Integer> getImageNumbers() {
        return imageNumbers;
    }
    
    /**
     * Stream ein Bild
     * @return
     * @throws FileNotFoundException
     * @throws URISyntaxException 
     */
    public StreamedContent getImage() throws FileNotFoundException, URISyntaxException {
        if (FacesContext.getCurrentInstance().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        }
        else {
            String i = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("imageID");
            StreamedContent stream = null;
            Picture picture = loginBean.anon.isPicturePublic(Integer.parseInt(i));
            if (picture != null) {               
                stream = readFile(picture, false);
            }
            else {
                picture = loginBean.usr.ownsPicture(i);
                if (picture != null) {
                    stream = readFile(picture, false);
                }
                else {
                    stream = new DefaultStreamedContent();
                }
            }     
            return stream;
        }
    }
    
    public StreamedContent getThumbnail() throws FileNotFoundException, URISyntaxException {
        if (FacesContext.getCurrentInstance().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        }
        else {
            String i = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("imageID");
            StreamedContent stream = null;
            Picture picture = loginBean.anon.isPicturePublic(Integer.parseInt(i));
            if (picture != null) {               
                stream = readFile(picture, true);
            }
            else {
                picture = loginBean.usr.ownsPicture(i);
                if (picture != null) {
                    stream = readFile(picture, true);
                }
                else {
                    stream = new DefaultStreamedContent();
                }
            }     
            return stream;
        }
    }
    
    private StreamedContent readFile(Picture picture, boolean thumbnail) {
        try {
            String dir;
            if (thumbnail) {
                dir = "../pictures/thumbnails/";
            }else {
                dir = "../pictures/";
            }
            String filename = Integer.toString(picture.getFilename()) + "." + picture.getFiletype();
            File pictureFile = new File(dir + filename); 
            return new DefaultStreamedContent(new FileInputStream(pictureFile), "image/" + picture.getFiletype().toLowerCase(), filename);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new DefaultStreamedContent();
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
    
    public String deleteImage(int pictureId) {
        loginBean.usr.deletePicture(pictureId);
        loginBean.refresh();
        init();
        return "pictures";
    }
}
    
