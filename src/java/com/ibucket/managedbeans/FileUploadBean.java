package com.ibucket.managedbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.primefaces.event.FileUploadEvent;

/**
 * Bean für den Bilderupload
 * 
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@ManagedBean(name="fileUploadBean")
@RequestScoped
public class FileUploadBean {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    
    /**
     * Creates a new instance of FileUploadBean
     */
    public FileUploadBean() {
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
    
    /**
     * Lädt ein Bild hoch
     * @param event 
     */
    public void upload(FileUploadEvent event) {
        if (event != null) {
            loginBean.usr.uploadPicture(event);
            loginBean.refresh();
        }
    }
}
