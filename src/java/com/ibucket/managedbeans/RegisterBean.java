package com.ibucket.managedbeans;


import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 * 
 * Bean für die Benutzerregistrierung
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@ManagedBean(name = "registerBean")
@RequestScoped
public class RegisterBean {

    @ManagedProperty( value = "#{loginBean}" )
    private LoginBean loginBean;
    /**
     * Creates a new instance of RegisterBean
     */
    private String usrName;
    private String pwd;
    private String pwdCheck;
    private String email;

    public RegisterBean() {
    }

    public String getPwdCheck() {
        return pwdCheck;
    }

    public void setPwdCheck(String pwdCheck) {
        this.pwdCheck = pwdCheck;
    }
    
    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    public String getPwd() {
        return null;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    /**
     * Registriert einen neuen Benutzer
     * @return String Link zur weiterführende Seite
     */
    public String registerAction()
    {
        if(pwd != null && pwd.equals(pwdCheck)) {
            if (usrName.length() < 4) {
                FacesContext ctx = FacesContext.getCurrentInstance();
                ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Registrierung fehlgeschlagen!","Bitte verwenden Sie einen Nutzernamen mit wenigstens 4 Zeichen."));
                return "";
            } else if (pwd.length() < 4) {
                FacesContext ctx = FacesContext.getCurrentInstance();
                ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Registrierung fehlgeschlagen!","Bitte verwenden Sie ein Passwort mit wenigstens 4 Zeichen."));
                return "";
            } else if(loginBean.anon.register(usrName, pwd, email) == true) {
                FacesContext ctx = FacesContext.getCurrentInstance();
                ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Registrierung erfolgreich!","Sie werden automatisch angemeldet."));
                loginBean.logon = usrName;
                loginBean.password = pwd;
                loginBean.loginAction();
                return "index";
            } else {
                FacesContext ctx = FacesContext.getCurrentInstance();
                ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Registrierung fehlgeschlagen!","Bitte Registrierungsdaten überprüfen!"));

                return "";
            }
        }
        else 
        {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Registrierung fehlgeschlagen!","Bitte Passwort kontrollieren!"));
            return "";
        }
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }    
}
