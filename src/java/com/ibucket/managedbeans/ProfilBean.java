package com.ibucket.managedbeans;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 * 
 * Beans für die Profilseite
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@ManagedBean(name = "profilBean")
@SessionScoped
public class ProfilBean implements Serializable {
    
    @ManagedProperty( value = "#{loginBean}" )
    private LoginBean loginBean;

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
    
    private String newPassCheck;
    private String newPass;
    private String oldPass;
    
    private String newEmailCheck;
    private String newEmail;

    public String getNewEmailCheck() {
        return newEmailCheck;
    }

    public void setNewEmailCheck(String newEmailCheck) {
        this.newEmailCheck = newEmailCheck;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }
    
    public String getOldPass() {
        return oldPass;
    }
    
    public String getNewPassCheck() {
        return newPassCheck;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public void setNewPassCheck(String newPassCheck) {
        this.newPassCheck = newPassCheck;
    }
      
    /**
     * Creates a new instance of ProfilBean
     */
    public ProfilBean() {
         
    }
    
    /**
     * ändert ein Passwort
     * @return boolean
     */
    public boolean setNewPasswort() {
        
        FacesContext ctx = FacesContext.getCurrentInstance();
        if(loginBean.usr.changePassword(newPass, oldPass, newPassCheck)){
            loginBean.setPassword(newPass);
            loginBean.refresh();
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Passwortänderung erfolgreich!",""));     
            newPass = "";
            oldPass = "";
            newPassCheck = "";
            return true;
        }else{
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Passwort ändern fehlgeschlagen!","Bitte prüfen Sie ihre Eingaben."));
            return false;
        }
    }
    
    /**
     * ändert die Emailadresse
     * @return boolean
     */
    public boolean setNewEmail() {
        
        FacesContext ctx = FacesContext.getCurrentInstance();
        if(loginBean.usr.changeEmail(newEmail, newEmailCheck, oldPass)){
            loginBean.refresh();
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"E-Mail-Änderung erfolgreich!",""));     
            newEmail = "";
            newEmailCheck = "";
            oldPass = "";
            return true;
        }else{
            ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"E-Mail ändern fehlgeschlagen!","Bitte prüfen Sie ihre Eingaben."));
            return false;
        }
    }
    
    public void changePasswordAction(){
        this.setNewPasswort();
    }
    
    public void changeEmailAction(){
            this.setNewEmail();
    }
}
