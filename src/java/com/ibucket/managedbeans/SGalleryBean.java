package com.ibucket.managedbeans;

import com.ibucket.persistence.Gallery;
import com.ibucket.persistence.Picture;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * 
 * Bean für einzele Gallerie
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@ManagedBean(name = "sGalleryBean")
@RequestScoped
public class SGalleryBean {

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    private String galleryName;
    private String galleryId;
    private Gallery gallery;
    private List<String> galleryNumbers;
    private Map<String, String> imageDescriptions;
    
    /**
     * Creates a new instance of SGalleryBean
     */
    public SGalleryBean() {

    }
    
    /**
     * prüft Zugriffsrechte und initialisiert die Objecte
     */
    @PostConstruct
    private void init() {
        galleryId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("galleryID");    
        if (galleryId != null) {
            gallery = loginBean.anon.isGalleryPublic(Integer.parseInt(galleryId));
            if (gallery == null && !loginBean.isNotLoggedIn()) {
                List<Gallery> galleries = loginBean.usr.getAllGalleries();
                for (Gallery g : galleries) {
                    if (Objects.equals(g.getGalleryId(), Integer.valueOf(galleryId))) {
                        gallery = g;
                        break;
                    }
                }
            }
            if (gallery != null) {
                galleryNumbers = new ArrayList<>();
                imageDescriptions = new HashMap<>();
                List<Picture> list = new ArrayList<>(gallery.getPictureCollection());
                Collections.sort(list, new Comparator<Picture>() {
                    @Override
                    public int compare(Picture p1, Picture p2) {
                        return p1.getNr()-p2.getNr();
                    }
                });
                for (Picture p : list) {
                    galleryNumbers.add(Integer.toString(p.getPictureId()));
                    imageDescriptions.put(Integer.toString(p.getPictureId()), p.getDescription());
                }
                galleryName = gallery.getName();
            }
        }
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public String getDescription() {
    return gallery.getDescription();
}
    
    public String getImageDescription(String id) {
        return imageDescriptions.get(id);
    }
    
    public String getGalleryName() {
        return galleryName;
    }

    public void setGalleryName(String galleryName) {
        this.galleryName = galleryName;
    }

    public List<String> getGalleryNumbers() {
        return galleryNumbers;
    }

    public void setGalleryNumbers(List<String> galleryNumbers) {
        this.galleryNumbers = galleryNumbers;
    }

    public String getGalleryId() {
        return galleryId;
    }

    public void setGalleryId(String galleryId) {
        this.galleryId = galleryId;
    }

    public Gallery getGallery() {
        return gallery;
    }

    public void setGallery(Gallery gallery) {
        this.gallery = gallery;
    }

    /**
     * Stream ein Bild
     * @param picture
     * @return 
     */
    private StreamedContent readFile(Picture picture) {
        try {
            String filename = Integer.toString(picture.getFilename()) + "." + picture.getFiletype();
            File pictureFile = new File("../pictures/" + filename); 
            return new DefaultStreamedContent(new FileInputStream(pictureFile), "image/" + picture.getFiletype().toLowerCase(), filename);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ImageBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new DefaultStreamedContent();
    }
    
    /**
     * holt ein Bild
     * @return Bild
     */
    public StreamedContent getImage() {
        if (FacesContext.getCurrentInstance().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            return new DefaultStreamedContent();
        }
        else {
            String imageId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("imageID");
            Picture p = loginBean.anon.isPicturePublic(Integer.parseInt(imageId));
            if (p == null) {
                p = loginBean.usr.ownsPicture(imageId);
            }
            return readFile(p);
        }
    }
    
    /**
     * prüft ob eine Gallerie sichtbar ist
     * @return boolean
     */
    public boolean canShow() {
        return gallery != null;
    }
}
