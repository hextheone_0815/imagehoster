package com.ibucket.businesslayer;

import static com.ibucket.businesslayer.Auth.salt;
import com.ibucket.persistence.Gallery;
import com.ibucket.persistence.Picture;
import com.ibucket.persistence.User;
import com.ibucket.persistence.databaseutil.DatabaseUtilRemote;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import org.primefaces.event.FileUploadEvent;

/**
 * Stellt funktionalität eines Angemeldeten Benutzers bereit
 * 
 * 
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
public class AuthUser {
    /**
     * Sperrobjekt für Datenbankzugriff
     */
    private static final Object lock = new Object();

    /**
     * Benutzerobjekt mit Benutzerinformationen aus der Datenbank
     */
    private User usrOBJ;
    
    /**
     * Datenbankutilitys
     */
    DatabaseUtilRemote dbu;

    /**
     * Erzeugt einen Authentifizierten Benutzer mit den BEnutzerinformationen aus der DB und Initialisiert die Datenbankutilitys
     * @param usrOBJ das Datenbankuserobjekt
     */
    public AuthUser(User usrOBJ) {
        this.usrOBJ = usrOBJ;

        try {
            Context ctx = new InitialContext();
            dbu = (DatabaseUtilRemote) ctx.lookup("ImageHosterEJB");
        } catch (Exception e) {

        }
    }

    /**
     * Hochladen von Bildern
     * @param event Eventobjekt mit Dateiinformationen
     */
    public void uploadPicture(FileUploadEvent event) {
        FileIOUtil util = new FileIOUtil();
        String filename;
        
        synchronized (lock) {
            filename = util.createDBEntryForImage(event.getFile().getFileName(), usrOBJ, "");
        }
        
        util.processUploadedFile(event.getFile(), filename);
    }

    /**
     * Löschen eines Bildes des Benutzerss
     * @param pictureId BildID
     * @return true wenn Löschen erfolgreich
     */
    public boolean deletePicture(int pictureId) {
        Picture toDelete = dbu.findPictureByPictureId(pictureId);
        if (toDelete.getOwningUser().equals(usrOBJ)) {
            if (toDelete.getGallery() != null) {
                dbu.removePictureFromGallery(pictureId, toDelete.getGallery().getGalleryId());
            }
            boolean returnValue = dbu.deletePicture(pictureId);

            if (returnValue == true) {
                FileIOUtil fileUtil = new FileIOUtil();
                fileUtil.deleteImage(toDelete.getFilename() + "." + toDelete.getFiletype());
            }

            return returnValue;
        } else {
            return false;
        }
    }

    /**
     * Veröffentlcihen eines Bildes
     * @param pictureId
     * @return true wenn Veröffentlichen erfolgreich
     * 
     */
    public boolean publishPicture(int pictureId) {
        Picture toDelete = dbu.findPictureByPictureId(pictureId);
        if (toDelete.getOwningUser().equals(usrOBJ)) {
            return dbu.publishPicture(pictureId);
        } else {
            return false;
        }

    }
/**
 * Bild Privat Stellen
 * @param pictureId
 * @return true wenn Erfolgreich
 */
    public boolean unpublishPicture(int pictureId) {
        Picture toDelete = dbu.findPictureByPictureId(pictureId);
        if (toDelete.getOwningUser().equals(usrOBJ)) {
            return dbu.unpublishPicture(pictureId);
        } else {
            return false;
        }
    }

    /**
     * Bild einer Galerie zuweisen
     * @param galleryId
     * @param pictureId 
     */
    public void changePictureGallery(int galleryId, int pictureId) {
        Gallery gal = dbu.findGalleryById(galleryId);
        Picture picture = dbu.findPictureByPictureId(pictureId);
        if ((gal != null && gal.getOwningUser().equals(usrOBJ) && picture != null && picture.getOwningUser().equals(usrOBJ)) || gal == null && picture != null && picture.getOwningUser().equals(usrOBJ)) {
            if (gal != null && gal.getVisibilityStatus().equals(DatabaseUtilRemote.PUBLIC)) {
                publishPicture(pictureId);
                picture = dbu.findPictureByPictureId(pictureId);
            }
            dbu.addPictureToGallery(picture, galleryId);
        }
    }

    /**
     * Bescheibung eines Bildes hinzufügen / ändern
     * @param pictureId
     * @param description
     * @return true wenn Erfolgreich
     */
    public boolean setPictureDescription(int pictureId, String description) {
        Picture pic = dbu.findPictureByPictureId(pictureId);
        if (pic != null && pic.getOwningUser().equals(usrOBJ)) {
            return dbu.setPictureDescription(pictureId, description);
        } else {
            return false;
        }

    }

    /**
     * Beschreibung eines Bildes holen
     * @param pictureId
     * @return Bildbeschreibung
     */
    public String getPictureDescription(int pictureId) {
        for (Iterator<Picture> it = usrOBJ.getPictureCollection().iterator(); it.hasNext();) {
            Picture picture = it.next();
            if (picture.getPictureId() == pictureId) {
                return picture.getDescription();
            } else {
                return "";
            }

        }
        return "";
    }

   /**
    * Galerie erstellen
    * @param galleryName Name der Galerie 
    * @return true wenn Erfolgreich
    */
    public boolean createGallery(String galleryName) {
        Gallery newGallery = new Gallery(null, galleryName, DatabaseUtilRemote.PRIVATE);
        newGallery.setDescription("");
        newGallery.setOwningUser(usrOBJ);

        boolean result = dbu.createGallery(newGallery);

        return result;
    }

    /**
     * Prüfung ob Aktueller User der Besitzer der Galerie ist
     * @param galleryId
     * @return true wenn Besitzer
     */
    public Gallery ownsGallery(int galleryId) {
        Collection<Gallery> imagelist = usrOBJ.getGalleryCollection();

        for (Iterator<Gallery> it = imagelist.iterator(); it.hasNext();) {
            Gallery gallery = it.next();
            if (gallery.getGalleryId() == galleryId) {
                return gallery;
            }
        }
        return null;
    }

    /**
     * Löschen einer Galerie
     * @param galleryId 
     */
    public void deleteGallery(int galleryId) {
        Gallery g = dbu.findGalleryById(galleryId);

        if (g != null && g.getOwningUser().equals(usrOBJ)) {
            dbu.deleteGallery(galleryId);
        }
    }

    /**
     * Veröffentlichen eienr Galerie
     * @param galleryId 
     */
    public void publishGallery(int galleryId) {
        Gallery g = dbu.findGalleryById(galleryId);

        if (g != null && g.getOwningUser().equals(usrOBJ)) {
            for (Picture p : g.getPictureCollection()) {
                publishPicture(p.getPictureId());
            }
            dbu.publishGallery(galleryId);
        }
    }

    /** 
     * Privatstellen einer Galerie
     * @param galleryId 
     */
    public void unpublishGallery(int galleryId) {
        Gallery g = dbu.findGalleryById(galleryId);

        if (g != null && g.getOwningUser().equals(usrOBJ)) {
            dbu.unpublishGallery(galleryId);
        }
    }

    /**
     * Beschreibung zu einer Galerie hinzufügen / ändern
     * @param galleryId
     * @param desc
     * @return 
     */
    public boolean setGalleryDescription(int galleryId, String desc) {
        Gallery gal = dbu.findGalleryById(galleryId);
        if (gal != null && gal.getOwningUser().equals(usrOBJ)) {
            return dbu.setGalleryDescription(galleryId, desc);
        } else {
            return false;
        }
    }

    
    /**
     * Galerie Beschreibung holen 
     * @param galleryId
     * @return Galleriebeschreibung oder eine leere Zeichenkette, wenn keine Beschreibung
     * gesetzt ist
     */
    public String getGalleryDescription(int galleryId) {
        for (Iterator<Gallery> it = usrOBJ.getGalleryCollection().iterator(); it.hasNext();) {
            Gallery gallery = it.next();
            if (gallery.getGalleryId() == galleryId) {
                return gallery.getDescription();
            } else {
                return "";
            }
        }
        return "";
    }

    /**
     * Liste mit Galerien holn
     * @return Liste der Galerieobjjekte
     */
    public List<Gallery> getAllGalleries() {
        return new ArrayList<>(usrOBJ.getGalleryCollection());
    }

    /**
     * Passwort Ändern
     * @param newPass Neues Passwort
     * @param oldPass Altes Passwort
     * @param newPassCheck Neues Passwort überprüfen
     * @return true wenn Erfolgreich
     */
    public boolean changePassword(String newPass, String oldPass, String newPassCheck) {

        if (PasswordUtil.generateHash(oldPass, salt+usrOBJ.getLogon()).equals(usrOBJ.getPassword())) {
            if (newPass.equals(newPassCheck)) {
                try {
                    DatabaseUtilRemote dbu;
                    Context ctx = new InitialContext();
                    dbu = (DatabaseUtilRemote) ctx.lookup("ImageHosterEJB");
                    dbu.changeUserPassword(usrOBJ.getUserId(), PasswordUtil.generateHash(newPass, salt+usrOBJ.getLogon()));

                } catch (Exception e) {
                    return false;
                }

                return true;

            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     * Email ändern
     * @param email Neue Email
     * @param emailCheck Neue Email überprüfung
     * @param pwd Benutzerpaswort
     * @return true wenn Erfolgreich
     */
    public boolean changeEmail(String email, String emailCheck, String pwd) {
        if (PasswordUtil.generateHash(pwd, salt+usrOBJ.getLogon()).equals(usrOBJ.getPassword())) {
            if (email.equals(emailCheck)) {
                try {
                    DatabaseUtilRemote dbu;
                    Context ctx = new InitialContext();
                    dbu = (DatabaseUtilRemote) ctx.lookup("ImageHosterEJB");
                    dbu.changeUserEmail(usrOBJ.getUserId(), email);

                } catch (Exception e) {
                    return false;
                }
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Liste aller Bilder des Benutzers holen
     * @return Liste mit Bilderobjekten
     */
    public List<Picture> getAllPictures() {
        return new ArrayList<>(usrOBJ.getPictureCollection());
    }

    
    /**
     * Prüfen ob Bilde dem Benutzer gehört
     * @param pictureID
     * @return true wenn Benutzer der Besitzer
     */
    public Picture ownsPicture(String pictureID) {
        Collection<Picture> imagelist = usrOBJ.getPictureCollection();

        for (Iterator<Picture> it = imagelist.iterator(); it.hasNext();) {
            Picture picture = it.next();
            if (picture.getPictureId()== Integer.parseInt(pictureID)) {
                return picture;
            }
        }
        return null;
    }

    /**
     * Abmelden
     * @return wenn Abmeldung Erfolgreich
     */
    public boolean logout() {
        usrOBJ = null;
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return true;
        
    }

    /**
     * Galerie Umbenennen
     * @param galleryId
     * @param galleryName neuer Name
     * @return true wenn Erfolgreich
     */
    public boolean setGalleryName(int galleryId, String galleryName) {
        Gallery gal = dbu.findGalleryById(galleryId);
        if (gal != null && gal.getOwningUser().equals(usrOBJ)) {
            return dbu.setGalleryName(galleryId, galleryName);
        } else {
            return false;
        }
    }

    
    public String getLogon() {
        return usrOBJ.getLogon();
    }

    public String getEmail() {
        return usrOBJ.getEmail();
    }
    /**
     * Bildrangfolge ändern
     * @param pictureId
     * @param nr
     * @return true wenn Erfolgreich
     */
    public boolean changePictureNumber(int pictureId, int nr) {
        return dbu.changePictureNr(pictureId, nr);
    }
}
