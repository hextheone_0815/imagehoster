package com.ibucket.businesslayer;

import com.ibucket.persistence.Gallery;
import com.ibucket.persistence.Picture;
import com.ibucket.persistence.databaseutil.DatabaseUtilRemote;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Repräsentiert einen anonymen Nutzer der Webanwendung und stellt die ihm zur 
 * Verfügung stehenden Funktionen bereit.
 * 
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
public class AnonUser {
    /** Ermöglicht Zugriff auf Datenbank */
    private DatabaseUtilRemote dbu;
    
    /**
     * Holt die Instanz der DatabaseUtil für den Datenbankzugriff vom AppServer
     */
    public AnonUser() {
        try {
            Context ctx = new InitialContext();
            dbu = (DatabaseUtilRemote) ctx.lookup("ImageHosterEJB");
        } catch (NamingException ex) {
            Logger.getLogger(AnonUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Versucht den Nutzer anzumelden. Bei erfolgreicher Anmeldung wird das Objekt des authentifizierten
     * Users zurückgeliefert.
     * 
     * @param uname Nutzername/Logon
     * @param pwd Passwort (klar)
     * @return das erstellte AuthUser-Objekt bei erfolgreicher anmeldung, andernfalls null
     */
    public AuthUser login(String uname,String pwd)
    {
        return Auth.login(uname,pwd);
    }
    
    /**
     * Registriert einen Nutzer mit den übergebenen Werten.
     * 
     * @param usrname Nutzername/Logon
     * @param passwort Passwort (klar)
     * @param email E-Mail-Adresse
     * @return true, bei erfolgreicher Registrierung, andernfalls false
     */
    public Boolean register(String usrname,String passwort,String email)
    {        
        return Auth.register(usrname, passwort, email);
    }
    
    /**
     * Prüft, ob das Bild mit der übergebenen Id öffentlich sichtbar ist. Sollte das 
     * Bild öffentlich sein, so wird das zugehörige Datenbankobjekt zurückgeliefert.
     * 
     * @param pictureId Id des zu prüfenden Bildes
     * @return das Datenbankobjekt des Bildes oder null, wenn das Bild nicht öffentlich ist
     */
    public Picture isPicturePublic(int pictureId) {
        Picture pic = dbu.findPictureByPictureId(pictureId);
        if (pic != null && Objects.equals(pic.getVisibilityStatus(), DatabaseUtilRemote.PUBLIC)) {
            return pic;
        }
        else {
            return null;
        }
    }
    
    /**
     * Prüft, ob die Galerie mit der übergebenen Id öffentlich sichtbar ist. Sollte die
     * Galerie öffentlich sein, so wird das zugehörige Datenbankobjekt zurückgeliefert.
     * 
     * @param galleryId Id der zu prüfenden Galerie
     * @return das Datenbankobjekt der Galerie oder null, wenn die Galerie nicht öffentlich ist
     */
    public Gallery isGalleryPublic(int galleryId){
        Gallery gal = dbu.findGalleryById(galleryId);
        if (gal != null && Objects.equals(gal.getVisibilityStatus(), DatabaseUtilRemote.PUBLIC)) {
            return gal;
        }
        else {
            return null;
        }
    }
}
