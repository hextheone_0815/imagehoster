package com.ibucket.businesslayer;

import com.ibucket.persistence.Picture;
import com.ibucket.persistence.User;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.UploadedFile;
import com.ibucket.persistence.databaseutil.DatabaseUtilRemote;
import java.io.InputStream;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import net.coobird.thumbnailator.name.Rename;
import net.coobird.thumbnailator.resizers.configurations.Antialiasing;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;


/**
 * Dient der Speicherung von hochgeladenen Dateien auf die Festplatte und der Eintragung
 * in die Datenbank. Außerdem können Dateien von der Festplatte der Servers gelöscht werden.
 *
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marco Schulze, Florian Vollmann
 */
public class FileIOUtil {
    /** Ermöglicht Zugriff auf Datenbank */
    private DatabaseUtilRemote dbu;
    
    /**
     * Initialisiert die Klassenvariablen.
     */
    public FileIOUtil() {
        try {
            Context ctx = new InitialContext();
            dbu = (DatabaseUtilRemote) ctx.lookup("ImageHosterEJB");
        } catch (NamingException ex) {
            Logger.getLogger(FileIOUtil.class.getName()).log(Level.SEVERE, null, ex);            
        }
    }
    
    /**
     * Speichert die hochgeladenen Dateien unter dem angegebenen Namen im Verzeichnis
     * &lt;Glassfish-Domainverzeichnis&gt;/pictures/ .
     * 
     * @param uploadedFile hochgeladenen Datei
     * @param name Name unter dem die Datei gespeichert werden soll
     * @return bei Erfolg true, andernfalls false
     */
    public boolean processUploadedFile(UploadedFile uploadedFile, String name) {
        boolean success = false;
        try {
            File file = new File("../pictures/" + name); 
            if (!file.exists())
            {
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                file.createNewFile();
            }
            
            InputStream input = uploadedFile.getInputstream();
            FileOutputStream output = new FileOutputStream(file);
            
            IOUtils.copyLarge(input, output);
            
            IOUtils.closeQuietly( input );
            IOUtils.closeQuietly( output );
            
            createThumbnail(file);
            
            success = true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIOUtil.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        } catch (IOException ex) {
            Logger.getLogger(FileIOUtil.class.getName()).log(Level.SEVERE, null, ex);
            success = false;
        }
        
        return success;
        
    }
    
    /**
     * Erzeugt für eine hochgeladene Datei einen Datenbankeintrag, der das Bild 
     * dem Besitzer zuordnet.
     * 
     * @param fileName Name der Datei
     * @param owner User-Objekt des Besitzers der Datei
     * @param desc Beschreibung der Datei
     * @return Dateiname im Format &lt;Bezeichnung&gt;.&lt;Endung&gt;
     */
    public String createDBEntryForImage(String fileName, User owner, String desc) {
        Picture pic = dbu.lastPicture();
        int name = pic!=null?pic.getFilename():0;
        Picture image = new Picture(null, 'p', 1, name+1, FilenameUtils.getExtension(fileName));
        image.setOwningUser(owner);
        image.setDescription(desc);  
        image.setGallery(null);

        dbu.addPicture(image);

        return Integer.toString(image.getFilename())+ "." + image.getFiletype();
    }
    
    /**
     * Löscht das Bild mit dem spezifizierten Namen von der Festplatte.
     * 
     * @param pictureName Name des zu löschenden Bildes
     */
    public void deleteImage(String pictureName) {
        File file = new File("../pictures/" + pictureName);
        file.delete();
        File fileThumb = new File("../pictures/thumbnails/" + pictureName);
        fileThumb.delete();
    }
    
    /**
     * Erzeugt für die hochgeladene Datei ein Thumbnail mit der Auflösung 250x250 Pixel. 
     * Dabei bleibt das ursprüngliche Dateiformat und das Apsekt-Verhältnis erhalten.
     * 
     * @param file das Originalbild
     * @return true, wenn das Thumbnail erfolgreich erstellt wurde, andernfalls false
     */
    private boolean createThumbnail(File file) {
        try {
            File dir = new File("../pictures/thumbnails/");
            if (!dir.exists()) {
                boolean success = dir.mkdir();
                if (!success) {
                    return false;
                }
            }
            
            Thumbnails.of(file).size(250, 250).antialiasing(Antialiasing.ON).
                    useOriginalFormat().allowOverwrite(true).
                    crop(Positions.CENTER).asFiles(dir, Rename.NO_CHANGE);
        } catch (IOException ex) {
            Logger.getLogger(FileIOUtil.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
