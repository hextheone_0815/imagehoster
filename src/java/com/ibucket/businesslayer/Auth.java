package com.ibucket.businesslayer;

import com.ibucket.managedbeans.LoginBean;
import com.ibucket.persistence.User;
import com.ibucket.persistence.databaseutil.DatabaseUtilRemote;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedProperty;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Funktionen zur Benutzerauthentifizierung und Registrierung 
 * 
 * 
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
public class Auth {
    
   
    
    @ManagedProperty( value = "#{loginBean}" )
    private LoginBean loginBean;
    static final String  salt = "ags-erfurt12345";
    
    /**
     * 
     * @param loginBean 
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }
    
    
    
     /**
     * Anmelden eines Registrierten Benutzers
     * @param usrname Benutzername
     * @param pwd Passwort
     * @return objekt des Angemeldeten Users
     */
    public static AuthUser login(String usrname, String pwd) {
        User usrOBJ = null;
        DatabaseUtilRemote dbu;
        try {
            Context ctx = new InitialContext();
            dbu = (DatabaseUtilRemote) ctx.lookup("ImageHosterEJB");
            usrOBJ = dbu.findUserByLogon(usrname);
        } catch (NamingException ex) {
            Logger.getLogger(Auth.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (usrOBJ != null) {
            
            String pwhash = PasswordUtil.generateHash(pwd, salt+usrOBJ.getLogon());
            
            if (usrOBJ.getPassword().equals(pwhash)){
                return new AuthUser(usrOBJ);
            }
            else
            {
                return null;
            }
        } else {
            return null;
        }

    }
    
 

    /**
     * Fügt zu registrierenden Benutzer in die Datenbank ein
     * 
     * Erzeugt den Passworthash und versucht den Benutzer in der Datenbank anzulegen
     * @param usrname Benutzername
     * @param passwort Klartextpasswort
     * @param email Emailadresse
     * @return true wenn das Anlegen erfolgreich war
     */
    public static Boolean register(String usrname,String passwort,String email)
    {
        User usrOBJ =null;
        User usrOBJ2 =null;
        String pwd = PasswordUtil.generateHash(passwort,salt+usrname);
        
        User newUSR = new User(null,usrname, pwd, email, new Date(), false);
        DatabaseUtilRemote dbu;
        try {
            Context ctx = new InitialContext();
            dbu = (DatabaseUtilRemote) ctx.lookup("ImageHosterEJB");
            usrOBJ = dbu.findUserByLogon(usrname);
            usrOBJ2 = dbu.findUserByEmail(email);
            if (usrOBJ == null && usrOBJ2 == null)
            {
                return dbu.addUser(newUSR);
            }
            else
            {
                return false;
            }
        } catch (NamingException ex) {
            Logger.getLogger(Auth.class.getName()).log(Level.SEVERE, null, ex);

            return false;
        }
                     
    }
}
