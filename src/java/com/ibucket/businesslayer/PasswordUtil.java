package com.ibucket.businesslayer;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Dient der Generierung von Passwort-Hashcodes nach SHA256.
 *
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marco Schulze, Florian Vollmann
 */
public class PasswordUtil {
    /**
     * Generiert einen SHA256 Hashcode aus dem übergebenen Passwort. Dabei wird 
     * das angegebene Crypto-Salt verwendet.
     * 
     * @param passwordToHash zu verschlüsselndes Passwort
     * @param salt zu verwendendes Crypto-Salt
     * @return erzeugter SHA256-Hash
     */
    public static String generateHash(String passwordToHash, String salt) {
        String generatedHash = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(salt.getBytes());
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedHash = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedHash;
    }
}
