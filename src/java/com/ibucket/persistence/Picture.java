package com.ibucket.persistence;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * Persitenceklasse für Bilder
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@Entity
@Table(name = "picture", catalog = "ibucket_db", schema = "")
@NamedQueries({
    @NamedQuery(name = "Picture.findAll", query = "SELECT p FROM Picture p"),
    @NamedQuery(name = "Picture.findByPictureId", query = "SELECT p FROM Picture p WHERE p.pictureId = :pictureId"),
    @NamedQuery(name = "Picture.findByVisibilityStatus", query = "SELECT p FROM Picture p WHERE p.visibilityStatus = :visibilityStatus"),
    @NamedQuery(name = "Picture.findByNr", query = "SELECT p FROM Picture p WHERE p.nr = :nr"),
    @NamedQuery(name = "Picture.findByFilename", query = "SELECT p FROM Picture p WHERE p.filename = :filename"),
    @NamedQuery(name = "Picture.findByDescription", query = "SELECT p FROM Picture p WHERE p.description = :description"),
    @NamedQuery(name = "Picture.findByFiletype", query = "SELECT p FROM Picture p WHERE p.filetype = :filetype")})

public class Picture implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pictureId")
    private Integer pictureId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "visibilityStatus")
    private Character visibilityStatus;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "nr")
    private int nr;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "filename")
    private int filename;
    
    @Size(max = 250)
    @Column(name = "description")
    private String description;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "filetype")
    private String filetype;
    
    @JoinColumn(name = "owningUser", referencedColumnName = "userId")
    @ManyToOne(optional = false)
    private User owningUser;
    
    @JoinColumn(name = "gallery", referencedColumnName = "galleryId")
    @ManyToOne
    private Gallery gallery;

    public Picture() {
    }

    public Picture(Integer pictureId) {
        this.pictureId = pictureId;
    }

    public Picture(Integer pictureId, Character visibilityStatus, int nr, int filename, String filetype) {
        this.pictureId = pictureId;
        this.visibilityStatus = visibilityStatus;
        this.nr = nr;
        this.filename = filename;
        this.filetype = filetype;
    }

    public Integer getPictureId() {
        return pictureId;
    }

    public void setPictureId(Integer pictureId) {
        this.pictureId = pictureId;
    }

    public Character getVisibilityStatus() {
        return visibilityStatus;
    }

    public void setVisibilityStatus(Character visibilityStatus) {
        this.visibilityStatus = visibilityStatus;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getFilename() {
        return filename;
    }

    public void setFilename(int filename) {
        this.filename = filename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public User getOwningUser() {
        return owningUser;
    }

    public void setOwningUser(User owningUser) {
        this.owningUser = owningUser;
    }

    public Gallery getGallery() {
        return gallery;
    }

    public void setGallery(Gallery gallery) {
        this.gallery = gallery;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pictureId != null ? pictureId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Picture)) {
            return false;
        }
        Picture other = (Picture) object;
        if ((this.pictureId == null && other.pictureId != null) || (this.pictureId != null && !this.pictureId.equals(other.pictureId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iBucket.persistence.Picture[ pictureId=" + pictureId + " ]";
    }
}
