package com.ibucket.persistence.databaseutil;

import com.ibucket.persistence.Gallery;
import com.ibucket.persistence.Picture;
import com.ibucket.persistence.User;
import javax.ejb.Remote;

/**
 * Interfaces für DatabaseUtil
 * 
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@Remote
public interface DatabaseUtilRemote {    
    public static final Character PUBLIC = 'ö';
    public static final Character PRIVATE = 'p';
    
    // User
    public User findUserByEmail(String email);
    public User findUserByLogon(String logon);
    public User findUserById(int userId);
    public boolean addUser(User user);
    public boolean changeUserPassword(int userId, String passwordHash);
    public boolean changeUserEmail(int userId, String email);
    public boolean deleteUserById(int userId);
    public boolean deleteUserByLogon(String logon);
    
    // Picture
    public Picture findPictureByPictureId(int pictureId);
    public Picture findPictureByFilename(String filename);
    public boolean publishPicture(int pictureId);
    public boolean unpublishPicture(int pictureId);
    public boolean setPictureDescription(int pictureId, String description);
    public boolean addPicture(Picture picture);
    public Picture lastPicture();
    public boolean deletePicture(int pictureId);
    public boolean changePictureNr(int pictureId, int nr);
    
    // Gallery
    public Gallery findGalleryById(int galleryId);
    public boolean createGallery(Gallery gallery);
    public boolean addPictureToGallery(Picture picture, int galleryId);
    public boolean removePictureFromGallery(int pictureId, int galleryId);
    public boolean deleteGallery(int galleryId);
    public boolean publishGallery(int galleryId);
    public boolean unpublishGallery(int galleryId);
    public boolean setGalleryDescription(int galleryId, String description);
    public boolean setGalleryName(int galleryId, String name);
}
