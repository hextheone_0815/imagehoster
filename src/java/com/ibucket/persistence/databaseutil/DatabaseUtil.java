package com.ibucket.persistence.databaseutil;

import com.ibucket.persistence.Gallery;
import com.ibucket.persistence.Picture;
import com.ibucket.persistence.User;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Datenbank Hilfsklasse
 * 
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@Stateless( mappedName = "ImageHosterEJB" )
public class DatabaseUtil implements DatabaseUtilRemote
{
   /**
    * EntityManager em: Ein Container-managed EntityManager der Peristence-Unit ProjLNDWPU.
    */
   @PersistenceContext( unitName = "ImageHosterPU" )
   private EntityManager em; 

    /**
     * holt User anhand der Email
     * @param email
     * @return User
     */
    @Override
    public User findUserByEmail(String email) {
        Query q = em.createNamedQuery("User.findByEmail")
                    .setParameter("email", email);
        User u;
        
        try {
            u = (User) q.getSingleResult();
            u.getGalleryCollection().size();
            for (Gallery g : u.getGalleryCollection()) {
                g.getPictureCollection().size();
            }
            u.getPictureCollection().size();
        }
        catch (NoResultException nre) {
            u = null;
        }
        
        return u;
    }

    /**
     * Holt die Profildaten des angemeldeten Users anhand des Logins
     * @param logon
     * @return User
     */
    @Override
    public User findUserByLogon(String logon) {
        Query q = em.createNamedQuery("User.findByLogon")
                    .setParameter("logon", logon);
        User u;
        
        try {
            u = (User) q.getSingleResult();
            u.getGalleryCollection().size();
            for (Gallery g : u.getGalleryCollection()) {
                g.getPictureCollection().size();
            }
            u.getPictureCollection().size();            
        }
        catch (NoResultException nre) {
            u = null;
        }
        
        return u;
    }
    /**
     * Holt die Profildaten des Users anhand der ID 
     * @param userId
     * @return User
     */
    @Override
    public User findUserById(int userId) {
        Query q = em.createNamedQuery("User.findByUserId")
                    .setParameter("userId", userId);
        User u;
        
        try {
            u = (User) q.getSingleResult();
            u.getGalleryCollection().size();
            for (Gallery g : u.getGalleryCollection()) {
                g.getPictureCollection().size();
            }
            u.getPictureCollection().size();            
        }
        catch (NoResultException nre) {
            u = null;
        }
        
        return u;
    }
    
    /**
     * fügt einen neuen Benutzer hinzu
     * @param user
     * @return bollean
     */
    @Override
    public boolean addUser(User user) {
        try {
            em.persist(user);            
            return true;
        }
        catch (Exception e) {
            System.err.println(e.toString());
            return false;
        }
        
    }

    /**
     * ändert das Passwort eines Users
     * @param userId
     * @param passwordHash
     * @return bollean
     */
    @Override
    public boolean changeUserPassword(int userId, String passwordHash) {
        User u = findUserById(userId);
        if (u != null) {
            u.setPassword(passwordHash);
            em.merge(u);
            em.flush();
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * ändert Benutzeremail
     * @param userId
     * @param email
     * @return bollean
     */
    @Override
    public boolean changeUserEmail(int userId, String email) {
        User u = findUserById(userId);
        if (u != null) {
            u.setEmail(email);
            em.merge(u);
            em.flush();
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * LÖscht USer anhand dessen UserID
     * @param userId
     * @return boolean
     */
    @Override
    public boolean deleteUserById(int userId) {
        try {
            User u = findUserById(userId);

            if (u != null) {
                em.remove(u);
            }
            
            return true;
        }
        catch(Exception e) {
            System.err.println(e.toString());
            return false;
        }
    }
    
    /**
     * löscht User anhand des Logon
     * @param logon
     * @return boolean
     */
    @Override
    public boolean deleteUserByLogon(String logon) {
        try {
            User u = findUserByLogon(logon);

            if (u != null) {
                em.remove(u);
            }
            return true;
        }
        catch(Exception e) {
            System.err.println(e.toString());
            return false;
        }
    }

    /**
     * holt ein Bild anhand dessen BildID
     * @param pictureId
     * @return Picture
     */
    @Override
    public Picture findPictureByPictureId(int pictureId) {
        Query q = em.createNamedQuery("Picture.findByPictureId")
                    .setParameter("pictureId", pictureId);
        Picture p;
        
        try {
            p = (Picture) q.getSingleResult();
        }
        catch (NoResultException nre) {
            p = null;
        }
        
        return p;        
    }
    
    /**
     * Holt ein Bild anhand dessen Dateinamens
     * @param filename
     * @return Picture
     */
    @Override
    public Picture findPictureByFilename(String filename) {
        Query q = em.createNamedQuery("Picture.findByFilename")
                    .setParameter("filename", Integer.parseInt(filename));
        Picture p;
        
        try {
            p = (Picture) q.getSingleResult();
        }
        catch (NoResultException nre) {
            p = null;
        }
        
        return p;     
    }

    /**
     * macht ein Bild öffentlich
     * @param pictureId
     * @return boolean
     */
    @Override
    public boolean publishPicture(int pictureId) {
        try {
            Picture p = findPictureByPictureId(pictureId);
            p.setVisibilityStatus(PUBLIC);
            em.merge(p);
            em.flush();
            return true;
        }
        catch(NoResultException e)
        {
            return false;
        }
    }

    /**
     * macht ein Bild wieder privat
     * @param pictureId
     * @return boolean
     */
    @Override
    public boolean unpublishPicture(int pictureId) {
        try {
            Picture p = findPictureByPictureId(pictureId);
            p.setVisibilityStatus(PRIVATE);
            em.merge(p);
            em.flush();
            return true;
        }
        catch(NoResultException e)
        {
            return false;
        }
    }

    /**
     * setzt eine Bildbeschreibung
     * @param pictureId
     * @param description
     * @return boolean
     */
    @Override
    public boolean setPictureDescription(int pictureId, String description) {
        Picture pic = findPictureByPictureId(pictureId);
        if (pic != null) {            
            pic.setDescription(description);
            em.merge(pic);
            em.flush();
            return true;
        }
        else {
            return false;
        }
        
    }    
    
    /**
     * fügt ein neues Bild hinzu
     * @param picture
     * @return boolean
     */
    @Override
    public boolean addPicture(Picture picture) {
        try {
            em.persist(picture);            
            return true;
        }
        catch (Exception e) {
            Logger.getLogger(DatabaseUtil.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }        
    }
    
    /**
     * holt das letzte Bild
     * @return Picture
     */
    @Override
    public Picture lastPicture() {
        Picture p;
        try {
            p = ( Picture ) em.createQuery( "SELECT p FROM Picture p WHERE p.filename = (SELECT MAX(pic.filename) FROM Picture pic)")
                    .getSingleResult();
        }
        catch(NullPointerException | NoResultException ex) {
            p = null;
        }
        
        return p;
    }
    
    /**
     * löscht ein Bild anhand der BildId
     * @param pictureId
     * @return boolean
     */
    @Override
    public boolean deletePicture(int pictureId) {
        try {
            Picture p = findPictureByPictureId(pictureId);

            if (p != null) {
                em.remove(p);
            }
            
            return true;
        }
        catch(Exception e) {
            System.err.println(e.toString());
            return false;
        }        
    }

    /**
     * ändert die Bildnummer
     * @param pictureId
     * @param nr
     * @return boolean
     */
    @Override
    public boolean changePictureNr(int pictureId, int nr) {
        try {
            Picture p = findPictureByPictureId(pictureId);

            if (p != null) {
                p.setNr(nr);
                em.merge(p);
                return true;
            }
            
            return false;
        }
        catch (Exception e) {
            System.err.println(e.toString());
            return false;
        }
    }        

    /**
     * holt Gallerie anhand der ID
     * @param galleryId
     * @return Gallery
     */
    @Override
    public Gallery findGalleryById(int galleryId) {
        Query q = em.createNamedQuery("Gallery.findByGalleryId")
                    .setParameter("galleryId", galleryId);
        Gallery g;
        
        try {
            g = (Gallery) q.getSingleResult();
            g.getPictureCollection().size();
        }
        catch (NoResultException nre) {
            g = null;
        }
        
        return g;                
    }

    /**
     * erstellt eine Gallerie
     * @param gallery
     * @return boolean
     */
    @Override
    public boolean createGallery(Gallery gallery) {
        try {
            em.persist(gallery);            
            return true;
        }
        catch (Exception e) {
            System.err.println(e.toString());
            return false;
        }
    }

    /**
     * fügt ein Bild einer Gallerie hinzu
     * @param picture
     * @param galleryId
     * @return boolean
     */
    @Override
    public boolean addPictureToGallery(Picture picture, int galleryId) {
        Gallery g = findGalleryById(galleryId);
        
        if (picture != null) {
            try {
                picture.setGallery(g);
                em.merge(picture);
                em.flush();
                return true;
            }
            catch (Exception e) {
                System.err.println(e.toString());
                return false;
            }
        }
        else {
            return false;
        }
    }

    /**
     * löscht ein Bild aus einer Gallerie
     * @param pictureId
     * @param galleryId
     * @return boolean
     */
    @Override
    public boolean removePictureFromGallery(int pictureId, int galleryId) {
        try {
            Gallery g = findGalleryById(galleryId);
            if (g != null) {
                Collection<Picture> pictures = g.getPictureCollection();
                
                if (pictures != null) {
                    for (Picture p : pictures) {
                        if (p.getPictureId() == pictureId) {
                            g.getPictureCollection().remove(p);
                            em.merge(g);
                            return true;
                        }
                    }
                }
            }   
            return false;
        }
        catch (Exception e) {
            System.err.println(e.toString());
            return false;
        }
    }
    
    /**
     * löscht eine Gallerie
     * @param galleryId
     * @return boolean
     */
   @Override
    public boolean deleteGallery(int galleryId) {
        Gallery g = findGalleryById(galleryId);
        
        if (g != null) {
            Collection<Picture> pictures = g.getPictureCollection();
                
            if (pictures != null) {
                for (Picture p : pictures) {
                    p.setGallery(null);
                    em.merge(p);
                }
            }
            em.flush();
            em.remove(g);
            return true;
        }
        else {
            return false;
        }                
    }
    
    /**
     * macht eine Gallerie öffentlich
     * @param galleryId
     * @return boolean
     */
   @Override
    public boolean publishGallery(int galleryId) {
        Gallery g = findGalleryById(galleryId);
        
        if (g != null) {
            g.setVisibilityStatus(PUBLIC);
            em.merge(g);
            em.flush();
            return true;
        }
        else {
            return false;
        }
    }
    
    /**
     * macht eine Gallerie privat
     * @param galleryId
     * @return boolean
     */
   @Override
    public boolean unpublishGallery(int galleryId) {
        Gallery g = findGalleryById(galleryId);
        
        if (g != null) {
            g.setVisibilityStatus(PRIVATE);
            em.merge(g);
            em.flush();
            return true;
        }
        else {
            return false;
        }        
    }
    
    /**
     * setzt eine galleriebeschreibung
     * @param galleryId
     * @param description
     * @return boolean
     */
   @Override
    public boolean setGalleryDescription(int galleryId, String description) {
        Gallery gal = findGalleryById(galleryId);
        if (gal != null) {            
            gal.setDescription(description);
            em.merge(gal);
            em.flush();
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * setzt einen Gallerienamen
     * @param galleryId
     * @param name
     * @return 
     */
    @Override
    public boolean setGalleryName(int galleryId, String name) {
        Gallery gal = findGalleryById(galleryId);
        if (gal != null) {            
            gal.setName(name);
            em.merge(gal);
            em.flush();
            return true;
        }
        else {
            return false;
        }
    }        
}
