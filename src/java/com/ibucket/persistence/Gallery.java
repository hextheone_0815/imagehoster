package com.ibucket.persistence;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * Persistenceklasse für Gallerie
 * @version 0.9b
 * @author Robert Werth, Roland Hantel, Marko Schulze, Florian Vollmann
 */
@Entity
@Table(name = "gallery", catalog = "ibucket_db", schema = "")
@NamedQueries({
    @NamedQuery(name = "Gallery.findAll", query = "SELECT g FROM Gallery g"),
    @NamedQuery(name = "Gallery.findByGalleryId", query = "SELECT g FROM Gallery g WHERE g.galleryId = :galleryId"),
    @NamedQuery(name = "Gallery.findByName", query = "SELECT g FROM Gallery g WHERE g.name = :name"),
    @NamedQuery(name = "Gallery.findByVisibilityStatus", query = "SELECT g FROM Gallery g WHERE g.visibilityStatus = :visibilityStatus"),
    @NamedQuery(name = "Gallery.findByDescription", query = "SELECT g FROM Gallery g WHERE g.description = :description")})

public class Gallery implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "galleryId")
    private Integer galleryId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "visibilityStatus")
    private Character visibilityStatus;
    
    @Size(max = 250)
    @Column(name = "description")
    private String description;
    
    @JoinColumn(name = "owningUser", referencedColumnName = "userId")
    @ManyToOne(optional = false)
    private User owningUser;
    
    @OneToMany(mappedBy = "gallery")
    private Collection<Picture> pictureCollection;

    public Gallery() {
    }

    public Gallery(Integer galleryId) {
        this.galleryId = galleryId;
    }

    public Gallery(Integer galleryId, String name, Character visibilityStatus) {
        this.galleryId = galleryId;
        this.name = name;
        this.visibilityStatus = visibilityStatus;
    }

    public Integer getGalleryId() {
        return galleryId;
    }

    public void setGalleryId(Integer galleryId) {
        this.galleryId = galleryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Character getVisibilityStatus() {
        return visibilityStatus;
    }

    public void setVisibilityStatus(Character visibilityStatus) {
        this.visibilityStatus = visibilityStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getOwningUser() {
        return owningUser;
    }

    public void setOwningUser(User owningUser) {
        this.owningUser = owningUser;
    }

    public Collection<Picture> getPictureCollection() {
        return pictureCollection;
    }

    public void setPictureCollection(Collection<Picture> pictureCollection) {
        this.pictureCollection = pictureCollection;
    }

    /**
     * Generiert den Hashcode
     * @return int 
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (galleryId != null ? galleryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gallery)) {
            return false;
        }
        Gallery other = (Gallery) object;
        if ((this.galleryId == null && other.galleryId != null) || (this.galleryId != null && !this.galleryId.equals(other.galleryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.iBucket.persistence.Gallery[ galleryId=" + galleryId + " ]";
    }
}
